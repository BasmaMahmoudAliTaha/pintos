#define F (1 << 12)

// X,Y FIXED POINT NUMBERS
// N INTEGER NUMBER
int convert_x_to_int(int x); // rounding
#define CONVERT_X_TO_INTGER(X) (X / F) //truncate
#define CONVERT_N_TO_FIXED_POINT(X) (X * F)
#define ADD_X_Y(X ,Y)(X + Y)
#define SUBTRACT_Y_X(X ,Y)(X - Y)   // X-Y
#define ADD_X_N(X ,N)(X + N * F)
#define SUBTRACT_N_X(X ,N)(X - N * F)  // SUBTRACT N FROM X
#define MULTIPLY_X_Y(X ,Y) ( (((int64_t)X) * Y) /F)
#define MULTIPLY_X_N(X ,N) (X * N)
#define DIVIDE_X_Y(X ,Y) ( (((int64_t)X) * F) /Y)
#define DIVIDE_X_N(X ,N) (X / N)
#define CONVERT_X_TO_INTGER_ROUNDING(X) ((X >= 0) ? ((X + F / 2) / F) : ((X - F / 2) / F))
/*
{
  if (x >= 0)
    {
      return (x + F / 2) / F;
    }
  else
    {
      return (x - F / 2) / F;
    }
}
*/
